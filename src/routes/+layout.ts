export const prerender = true;
export const trailingSlash = 'always';
import type { LayoutLoad } from './$types';

export const load: LayoutLoad = async ({ url }) => {
  return {
    props: {
      path: url.pathname,
    },
  };
};
