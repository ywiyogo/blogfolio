import type { BaseTranslation } from '../i18n-types';

const en: BaseTranslation = {
  Tags: 'Tags',
  FilterTags: 'Filter Tags...',
  TableOfContent: 'Table of Content',
  LoadingPosts: 'Loading Posts...',
  LoadingPost: 'Loading Post...',
  LoadingProjects: 'Loading Projects...',
  LoadingProject: 'Loading Project...',
  NoPostFound: 'No Post Found.',
  NoProjectFound: 'No Project Found.',
  LoadingGiscus: 'Loading Giscus...',
  BlogFolio: '🚀 BlogFolio [α] - Built with SvelteKit and ❤',
  FirstPublishedAt: 'First published at',
  LastUpdatedAt: 'Last updated at',
  Updated: 'Updated: ',
  JustNow: 'just now',
  MinuteAgo: '{0} minute{{s}} ago',
  HourAgo: '{0} hour{{s}} ago',
  DayAgo: '{0} day{{s}} ago',
  MonthAgo: '{0} month{{s}} ago',
  YearAgo: '{0} year{{s}} ago',
  Page404NotFound: 'Page Not Found',
  Page404BackHome: 'Go Back Home !',
  IndexSearchBox: 'Search',
  IndexCloseSearchBox: 'Close',
};

export default en;
