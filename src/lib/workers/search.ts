import type { Post } from '$lib/types/post';
import type { Project } from '$lib/types/project';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import postsjson from '$generated/posts.json';
import projectsjson from '$generated/projects.json';
import { search } from '$lib/search';

const articles = (() => {
  const _allposts = postsjson as [string, Post.Post][];
  const _allprojects = projectsjson as [string, Project.Project][];
  const _allarticles = _allposts.concat(_allprojects);
  const _articlesToShow = _allarticles
    .filter((e) => {
      return !(e[1]['options'] && e[1]['options'].includes('unlisted'));
    })
    .flatMap((e) => e[1]);
  return {
    data: _articlesToShow,
  };
})();

addEventListener('message', async (event) => {
  const { type, payload } = event.data;

  if (type === 'init') {
    search.init(articles.data);
    postMessage({ type: 'ready' });
  }

  if (type === 'query') {
    const result = search.search(payload);
    postMessage({ type: 'query', payload: result });
  }
});
