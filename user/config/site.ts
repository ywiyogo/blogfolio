import type { Site } from '$lib/types/site';
import type { Giscus } from '$lib/types/giscus';
import type { DD } from '$lib/types/dd';

import Avatar from '$assets/avatar.png';
import Avatar_128 from '$assets/avatar.png?w=128&h=128&format=avif;webp&imagetools';
import Avatar_48_PNG from '$assets/avatar.png?w=48&h=48&imagetools';
import Avatar_96_PNG from '$assets/avatar.png?w=96&h=96&imagetools';
import Avatar_192_PNG from '$assets/avatar.png?w=192&h=192&imagetools';
import Avatar_512_PNG from '$assets/avatar.png?w=512&h=512&imagetools';

import SiteCover from '$assets/preview.avif';

export const siteConfig: Site.Config = {
  url: 'https://ywiyogo.github.io/BlogFolio',
  title: 'BlogFolio',
  subtitle: '🚀 BlogFolio - Built using Svelte',
  description: '🚀 BlogFolio - Blog and Portfolio build starter, built using Svelte',
  typed_title1: 'Modern professional template',
  typed_title2: 'for',
  typed_strings: ['freelancers', 'bloggers', 'startups', 'small business', 'online shops'],
  slogan: 'Creating values through open source software development.',
  lang: 'en',
  timeZone: 'Europe/Berlin',
  since: 2023,
  cover: SiteCover,
  author: {
    name: 'Tony Yoe',
    status: '🚀',
    statusTip:
      '<a href="https://gitlab.com/ywiyogo/blogfolio" rel="external" style="color:#0F0" onMouseOver="this.style.color=\'#0FF\'" onMouseOut="this.style.color=\'#0F0\'" >BlogFolio</a> is Awesome !',
    avatar: Avatar,
    avatar_128: Avatar_128,
    avatar_48_png: Avatar_48_PNG,
    avatar_96_png: Avatar_96_PNG,
    avatar_192_png: Avatar_192_PNG,
    avatar_512_png: Avatar_512_PNG,
    website: 'https://gitlab.com/ywiyogo/BlogFolio',
    gitlab: 'https://gitlab.com/ywiyogo',
    twitter: '',
    email: '',
    bio: `Peace begins <br/> With a smile`,
  },
  // Let it empty if you don't use GitlabPages or adapt it to the repository name
  gitlabPages: 'blogfolio',
};

export const headConfig: Site.Head = {
  // Used for IndieWeb
  me: ['https://gitlab.com/ywiyogo'],
  custom: ({ dev }) =>
    dev
      ? [
          // For Development Environment
        ]
      : [
          // For Production Environment

          // Replace the following with your own setting

          // Plausible

          // Fill the Google tag
          `<script type="text/partytown" src="https://www.googletagmanager.com/gtag/js?id="></script>`,
          `<script type="text/partytown">
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-LQ73GWF6XT');
          </script>`,
        ],
};

export const dateConfig: Site.DateConfig = {
  toPublishedString: {
    locales: 'en-US',
    options: {
      year: 'numeric',
      weekday: 'short',
      month: 'short',
      day: 'numeric',
      timeZone: `${siteConfig.timeZone}`,
    },
  },
  toUpdatedString: {
    locales: 'en-US',
    options: {
      year: 'numeric',
      weekday: 'short',
      month: 'short',
      day: 'numeric',
      timeZone: `${siteConfig.timeZone}`,
    },
  },
};

// Replace with your own Giscus setting
// See https://giscus.app/
export const giscusConfig: Giscus.Config = {
  enable: true,
  id: 'giscus-comment',
  repo: import.meta.env.BlogFolio_GISCUS_REPO,
  repoId: import.meta.env.BlogFolio_GISCUS_REPO_ID,
  category: import.meta.env.BlogFolio_GISCUS_CATEGORY,
  categoryId: import.meta.env.BlogFolio_GISCUS_CATEGORY_ID,
  mapping: 'pathname',
  reactionsEnabled: '1',
  emitMetadata: '0',
  inputPosition: 'top',
  loading: 'lazy',
  lang: 'en',
  'data-strict': '0',
};

export const navConfig: (DD.Nav | DD.Link)[] = [
  {
    name: 'Blog',
    url: '/blogs',
  },
  {
    name: 'Projects',
    url: '/projects',
  },
  {
    name: 'About',
    url: '/about',
  },
];

export const mobilenavConfig: DD.Nav = {
  orientation: 2,
  links: [
    {
      name: 'Blog',
      url: '/blogs',
    },
    {
      name: 'Projects',
      url: '/projects',
    },
    {
      name: 'About',
      url: '/about',
    },
  ],
};
