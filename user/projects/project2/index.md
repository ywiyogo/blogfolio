---
title: Project 2
description: Demonstrates basics of BlogFolio - Simply Awesome Blog Starter. Built using SvelteKit and Love.
summary: ✨ Just a QuickStart guide
published: '2022-08-08T00:00:00.000+08:00'
updated: '2022-09-15T21:00:00.000+08:00'
cover: ./cover.jpg
coverStyle: 'TOP'
coverCaption: Photo by <a href="https://unsplash.com/ja/@mxhpics?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Maxime Horlaville</a> on <a href="https://unsplash.com/s/photos/start?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
tags:
  - [BlogFolio]
---

## ✨ Intro

[BlogFolio](https://gitlab.com/ywiyogo/blogfolio) is simply an Awesoem blog starter and static site generator. Built using [SvelteKit](https://kit.svelte.dev/) and Love ❤.

## 🎉 Try out BlogFolio

Create a BlogFolio blog named `my-blog`.

```sh
git clone https://gitlab.com/ywiyogo/blogfolio my-page
```

## ⚡️ To Dev

1. Install all the dependencies.

   ```sh
   pnpm i
   ```

1. Start local dev server.

   ```sh
   pnpm dev
   ```

1. Sever is running. Open browser to see the result.

   ```sh
   VITE v3.1.1  ready in 1080 ms

   ➜  Local:   http://localhost:5173/
   ➜  Network: use --host to expose
   ```

## 🚀 To Deploy

1. Create a free [Vercel](https://vercel.com/) account.

1. Install Vercel CLI

   ```sh
   npm i -g vercel
   ```

1. Using the vercel command from the root of a project directory.

   ```sh
   vercel
   ```

   You will be asked to login for the first time. Simply follow the prompts.

1. Deploy as Production build

   ```sh
   vercel deploy --prod
   ```

## 👍 END
