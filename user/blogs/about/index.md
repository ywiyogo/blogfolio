---
title: About BlogFolio
description: Information about BlogFolio - Simply Awesome Blog Starter. Built using SvelteKit and Love.
published: '2022-08-20T00:00:00.000+08:00'
updated: '2022-09-16T12:00:00.000+08:00'
cover: /blogs/about/blogfolio.avif
options:
  - unlisted
---

## 🎉 BlogFolio

[BlogFolio](https://gitlab.com/ywiyogo/blogfolio) is a blog starter and static site generator, built with [Svelte](https://svelte.dev/) and [SvelteKit](https://kit.svelte.dev/), forked from svelte-QWER.

<ImgZoom src="/preview.avif" alt="BlogFolio Preview" />


## ✨ Features

- ⚡ Super fast site performance. Near **Perfect** [PageSpeed](https://pagespeed.web.dev/) score.

- 🤗 SEO ready with meta, [Open Graph](https://ogp.me/), [Schema](https://schema.org/), [JSON-LD](https://json-ld.org/), [microformats2](https://indieweb.org/microformats2).

- 🔎 On-site Search.

- 📱 Mobile-First / Responsive design.

- ✍️ Write post with [Markdown syntax](https://www.markdownguide.org/basic-syntax/).

- 🧮 🧪 Wirte Math and Chemical formulas with [TeX Syntax](https://www.math.brown.edu/johsilve/ReferenceCards/TeXRefCard.v1.5.pdf) via [Katex](https://katex.org/) & [mhchem](https://mhchem.github.io/MathJax-mhchem/).

- 📁 Folder-based organization for creating posts.

- 🏷️ Multi-Tags filtering.

- 📄 Auto-Generate Table of Content for posts.

- 📦 Out-of-the-box support for [Atom feed](https://validator.w3.org/feed/docs/atom.html), Sitemap, [PWA](https://web.dev/progressive-web-apps/), [JSON Feed](https://www.jsonfeed.org/).

- 🖼️ Automatic image optimization via [vite-imagetools](https://github.com/JonasKruckenberg/imagetools).

- ⚙️ Support embedding Svelte components and Javascript with Markdown.

- 💄 Utilizes [UnoCSS](https://github.com/unocss/unocss) - the instant on-demand atomic CSS engine.

- 🌐 i18n via [typesafe-i18n](https://github.com/ivanhofer/typesafe-i18n).

- 🚀 Deploy the blog **Free** on [Vercel](https://vercel.com/) or [Netlify](https://Netlify.com/).

## 📚 Learn More

Please read more at our [Documentation](https://docs-svelte-BlogFolio.vercel.app/).

## 👷 Current State

[BlogFolio](https://gitlab.com/ywiyogo/blogfolio) is at its' very early stage. Kindly expect frequent breaking changes.

Just copy and paste your `user` folder to the latest release.

Watch out for the CHANGELOG to see if yor are required to migrate `user/config` or other places.

## ❓ Issues / Problems / Questions

Please take advantage of our [Github's Q&A Discussion board](https://gitlab.com/ywiyogo/blogfolio/discussions/categories/q-a).


## 📝 License

[MIT](https://gitlab.com/ywiyogo/blogfolio/-/blob/main/LICENSE)

## 🙏 Credits

- Cover Photo by <a href="https://unsplash.com/@jessbaileydesigns?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Jess Bailey</a> on <a href="https://unsplash.com/s/photos/note?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

- Avatar Illustration by <a href="https://icons8.com/illustrations/author/GrbQqWBEhaDS">Liam Moore</a> from <a href="https://icons8.com/illustrations">Ouch!</a>

## 👍 END

Thanks so much for interesting in [BlogFolio](https://gitlab.com/ywiyogo/blogfolio). Please try it out and let me know what you think 😊.
