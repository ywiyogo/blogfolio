export const projects = (() => {
  let _projects = new Map();

  const sortByPublishedDateLatestFirst = (a, b) => {
    const da = new Date(a[1]['published']);
    const db = new Date(b[1]['published']);
    if (db == da) return 0;
    if (db > da) return 1;
    return -1;
  };

  return {
    set: (slug, value) => {
      _projects.set(slug, value);
    },
    get: (slug) => {
      return _projects.get(slug);
    },
    delete: (slug) => {
      _projects.delete(slug);
    },
    raw: () => {
      return _projects;
    },
    clear: () => {
      _projects.clear();
    },
    json: () => {
      const listedProject = Array.from(_projects).filter((e) => {
        return !(e[1]['options'] && e[1]['options'].includes('unlisted'));
      });
      listedProject.sort(sortByPublishedDateLatestFirst);

      for (let i = 0; i < listedProject.length; i += 1) {
        const prev = i + 1 < listedProject.length ? listedProject[i + 1][1].slug : undefined;
        const next = i - 1 >= 0 ? listedProject[i - 1][1].slug : undefined;
        _projects.set(listedProject[i][0], { ...listedProject[i][1], prev: prev, next: next });
      }

      return JSON.stringify(Array.from(_projects).sort(sortByPublishedDateLatestFirst));
    },
    read: (str) => {
      const _input = JSON.parse(str);
      _projects.clear();
      _projects = new Map(Object.entries(_input));
    },
  };
})();
