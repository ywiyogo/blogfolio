import adapterNode from '@sveltejs/adapter-node';
import adapterStatic from '@sveltejs/adapter-static';
import adapterNetlify from '@sveltejs/adapter-netlify';
import preprocess from 'svelte-preprocess';

const dev = process.argv.includes('dev');

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: preprocess({ preserve: ['partytown'] }),
  kit: {
    adapter: Object.keys(process.env).some((key) => key.includes('NETLIFY'))
      ? adapterNetlify()
      : process.env.ADAPTER === 'node'
      ? adapterNode({ out: 'build' })
      : adapterStatic({
          pages: 'build',
          assets: 'build',
          fallback: null,
        }),
    paths: {
      base: dev ? '' : process.env.BASE_PATH,
    },
    csp: { mode: 'auto' },
  },
};

export default config;
